﻿// kameran kiihtyminen levelsScenessä
public var acceleration : float;
public var maxSpeed : float;
public var animationStarted : boolean = false;
 
 public function Update() {
 	if(animationStarted)
 	{
 		transform.Translate(0, 0, acceleration * Time.deltaTime);
 		if (acceleration < maxSpeed) {
 			acceleration += 0.1;
 		} 
 
 	}
 }
 public function SetAnimationStart()
 {
 	animationStarted = true;
 }
 