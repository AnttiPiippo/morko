﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

public class TimeBar : MonoBehaviour {
	bool timerOn = false;
	public float timebar = 0.0f;
	public float kokoAikaBar = 1f;
	float  multiplier = 0.0f;
 
    void Start () {

    }

    public void Update () {

		if (timerOn ) {

			timebar += Time.deltaTime;

			if (timebar < 0.0f) {
				timebar = 0.0f;
			}

			this.GetComponent<Image> ().fillAmount = Map (timebar* multiplier, 0, kokoAikaBar, 0, 1);
		}
    }

	public float Map(float value, float inMin, float inMax, float outMin, float outMax)
	{
		return (value - inMin) * (outMax - outMin) / (inMax - inMin) + outMin;
	}

	public void TimerOn(float length)
	{
		Debug.Log ("Length "+ length);
		if(length == 0.25)
		{
			multiplier = 0.125f;
		}
						
		if(length == 0.5)
		{
			multiplier = 0.25f;
		}
		
		if(length == 1)
		{
			multiplier = 0.5f;
		}
		
		if(length == 2)
		{
			multiplier = 1f;
		}
		
		if(length == 4)
		{
			multiplier = 2f;
		}

		timerOn = true;
		kokoAikaBar = length;
		timebar = 0.0f;
	} 

	public void TimerOff()
	{
		timerOn = false;
	}

    
   
    
}

