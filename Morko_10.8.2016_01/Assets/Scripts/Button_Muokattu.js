﻿#pragma strict
var WhatButton: int = 0;
var object : Transform;
var RightSound : GameObject;
var ErrorSound : GameObject;
var timerOn : boolean = false;
var startTime : float;
var time : float;
var noteType: GameObject;
var noteColliderName: String;
var noteLength : float;
var speed : float = 2.0;
var tweenObject : GameObject;
var errorMargin = 0.07f;
public var negPoints = 200;
var  ignoreRelease : boolean = false;

public var kokonuotti : float;            
public var puolinuotti : float;           
public var neljasosanuotti : float;       
public var kahdeksasosanuotti : float;

public var Hahmo : Animator;
var lastNoteInstanceID : int;

public function buttonPress() {
 
		if (noteType!=null && noteType.name == "kahdeksasosataukoVanha(Clone)"){
		    GameObject.Find("BarFull").SendMessage("changeToRed");
		    Maincode.score -= negPoints;
		    GameObject.Find("Wallpaper").SendMessage("OnceShake");
		}	
		if (noteType!=null && noteType.name == "neljasosataukoVanha(Clone)"){
		    GameObject.Find("BarFull").SendMessage("changeToRed");	
		    Maincode.score -= negPoints;
		    GameObject.Find("Wallpaper").SendMessage("OnceShake");

		}	
		if (noteType!=null && noteType.name == "PuolitaukoVanha(Clone)"){
		    GameObject.Find("BarFull").SendMessage("changeToRed");	
		    Maincode.score -= negPoints;
		    GameObject.Find("Wallpaper").SendMessage("OnceShake");

		}	
		if (noteType!=null && noteType.name == "kokotaukoVanha(Clone)"){	
		    GameObject.Find("BarFull").SendMessage("changeToRed");	
		    Maincode.score -= negPoints;
		    GameObject.Find("Wallpaper").SendMessage("OnceShake");
		}	

		if (noteType!=null && noteType.name == "timSpriteSheet_33(Clone)"){		
		    GameObject.Find("BarFull").SendMessage("changeToGreen");

		}
		if (noteType!=null && noteType.name == "kokonuottiVanha(Clone)"){		
		    GameObject.Find("BarFull").SendMessage("changeToGreen");
		    
		}	
		if (noteType!=null && noteType.name == "PuolinuottiVanha(Clone)"){	
		    GameObject.Find("BarFull").SendMessage("changeToGreen");
		     
		}	
		if (noteType!=null && noteType.name == "neljasosanuottiVanha(Clone)"){	
		    GameObject.Find("BarFull").SendMessage("changeToGreen");
		    
		}	
		
		if (noteType!=null && noteType.name == "kahdeksasosanuottiVanha(Clone)"){	
		    GameObject.Find("BarFull").SendMessage("changeToGreen"); 
		}
		 

    else if(noteType!=null && noteType.GetComponent(Note1).isClean == false)
    {
    	noteType.GetComponent(Note1).isMoving = false;
    	noteType.GetComponent(Note1).isAnimatin = true;
    	timerOn = true;    	
    	startTime=Time.time;
    	GameObject.Find("BarFull").SendMessage("TimerOn",noteLength);
    }
   
}

public function buttonReleased() {       
		
		if(noteType!=null && ignoreRelease==false)
		{
		    timerOn = false;
		    errorMargin = 0.07f;
				if(time >= noteLength-errorMargin && noteType.GetComponent(Note1).isClean == false)
				    {	
				        var errorTimeKN = Mathf.Abs(time-noteLength);
				        var pointsKN = 100*(1-errorTimeKN);
				        Maincode.score += pointsKN;   

				        if(noteType!=null && noteType.name == "kokonuottiVanha(Clone)")  // "kokonuottiVanha(Clone)"
				        {
				            //   noteType.SendMessage("");
				        }
				        if (noteType!=null && noteType.name == "kokonuottiVanha(Clone)")
				        {
				            //   noteType.SendMessage("");
				        }
                            
				        if(noteType!=null && noteType.name == "PuolinuottiVanha(Clone)")  // "PuolinuottiVanha(Clone)"
				        {
				            //   noteType.SendMessage("");
				        }
				        if(noteType!=null && noteType.name == "PuolinuottiVanha(Clone)")
				        {
				            //   noteType.SendMessage("");
				        }

				        if(noteType!=null && noteType.name == "neljasosanuottiVanha(Clone)")  // "neljasosanuottiVanha(Clone)"
				        {
				            //   noteType.SendMessage("");
				        }
				        if(noteType!=null && noteType.name == "neljasosanuottiVanha(Clone)")  // "neljasosanuottiVanha(Clone)"
				        {
				            //   noteType.SendMessage("");
				        }
				        if(noteType!=null && noteType.name == "KahdeksasosanuottiVanha(Clone)")  // "KahdeksasosanuottiVanha(Clone)"  
				        {
				         //   noteType.SendMessage("");
				        }
				        /*
				        if(noteType!=null && noteType.name == "timSpriteSheet_33(Clone)"  && pointsKN<=98.90)  //  && pointsKN==0
				        {
				           noteType.SendMessage("Sad"); 
				        }
                        */
				        if(noteType!=null && noteType.name == "timSpriteSheet_33(Clone)") 
				        {
				           noteType.SendMessage("Happy"); 
				        }
				
				}
		    else
				{
				    noteType.SendMessage("Sad"); 
				}
			if (noteType.name != "kahdeksasosataukoVanha(Clone)")			
				{
				noteType.GetComponent(Note1).isAnimatin = false;
				noteType.GetComponent(Note1).isMoving = true;
				GameObject.Find("BarFull").SendMessage("TimerOff");	

				}
		    if (noteType.name != "neljasosataukoVanha(Clone)")			
				{
		        noteType.GetComponent(Note1).isAnimatin = false;
				noteType.GetComponent(Note1).isMoving = true;
				GameObject.Find("BarFull").SendMessage("TimerOff");
				}
		    if (noteType.name != "PuolitaukoVanha(Clone)")			
				{
		        noteType.GetComponent(Note1).isAnimatin = false;
				noteType.GetComponent(Note1).isMoving = true;
				GameObject.Find("BarFull").SendMessage("TimerOff");
				}
		    if (noteType.name != "kokotaukoVanha(Clone)")			
				{
		        noteType.GetComponent(Note1).isAnimatin = false;
				noteType.GetComponent(Note1).isMoving = true;
				GameObject.Find("BarFull").SendMessage("TimerOff");	
				}
		
				if(noteColliderName == "Collider1")
				{
			    tweenObject = GameObject.Find("iTween1");
				tweenObject.SendMessage("setGameobjectForAnimation",noteType);
				tweenObject.SendMessage("playAnimation");
				
				}
				if(noteColliderName == "Collider2")
				{
				tweenObject = GameObject.Find("iTween2");
				tweenObject.SendMessage("setGameobjectForAnimation",noteType);
				tweenObject.SendMessage("playAnimation2");	
							
				}
				if(noteColliderName == "Collider3")
				{
				tweenObject = GameObject.Find("iTween3");
				tweenObject.SendMessage("setGameobjectForAnimation",noteType);
				tweenObject.SendMessage("playAnimation3");	
				
				}
				if(noteColliderName == "Collider4") 
				{
				tweenObject = GameObject.Find("iTween4");
				tweenObject.SendMessage("setGameobjectForAnimation",noteType);
				tweenObject.SendMessage("playAnimation4");	
				
				}			
				
		}
		
		else
		{
			ignoreRelease = false;
		}
		noteType=null;
		


		/*			
		if(noteColliderName == "Collider1") //kokonuotti
		{
     
     
     // GetComponent.<Animation>().SendMessage("playAnimation");
		errorMargin = 0.1f;
		var kokonuotti = 0.3f;
		
		
		
			if(time >= kokonuotti-errorMargin && time <= kokonuotti+errorMargin && noteType.GetComponent(Note1).isClean == false)
			{	
			    tweenObject = GameObject.Find("iTween1");
			    tweenObject.SendMessage("playAnimation");
			    		     
			    var errorTimeKN = Mathf.Abs(time-kokonuotti);
			    var pointsKN = 100*(1-errorTimeKN);
			    Maincode.score += pointsKN; 
				Debug.Log("Add points = "+pointsKN);
			
		    }
		}
		else if(noteColliderName == "Collider2") //puolinuotti
		{
		errorMargin = 0.1f;
		var puolinuotti = 0.3f;
		
		   if(time >= puolinuotti-errorMargin && time <= puolinuotti+errorMargin && noteType.GetComponent(Note1).isClean == false)
		   {
		       tweenObject = GameObject.Find("iTween2");
		       tweenObject.SendMessage("playAnimation2");
		      
		       var errorTimePN = Mathf.Abs(time-puolinuotti);
			   var pointsPN = 100*(1-errorTimePN);
			   Maincode.score += pointsPN; 
			   Debug.Log("Add points = "+pointsPN);
			   
			
		   }
		}
		else if(noteColliderName == "Collider3") //neljäsosanuotti
		{
		errorMargin = 0.1f;
		var neljasosanuotti = 0.3f;
		
			if(time >= neljasosanuotti-errorMargin && time <= neljasosanuotti+errorMargin && noteType.GetComponent(Note1).isClean == false)  
			{     
			   
		        tweenObject = GameObject.Find("iTween3");
		        tweenObject.SendMessage("playAnimation3");
		        
		        var errorTimeNN = Mathf.Abs(time-neljasosanuotti);
			    var pointsNN = 100*(1-errorTimeNN);
			    Maincode.score += pointsNN; 
				Debug.Log("Add points = "+pointsNN);
				
			
		    }
		}
		
		else if(noteColliderName == "Collider4") //kahdeksasosanuotti
		{	
		errorMargin = 0.1f;
		var kahdeksasosanuotti = 0.3f;
		
			if(time >= kahdeksasosanuotti-errorMargin && time <= kahdeksasosanuotti+errorMargin && noteType.GetComponent(Note1).isClean == false)
			{
			 
		       tweenObject = GameObject.Find("iTween4");
		       tweenObject.SendMessage("playAnimation4");
		       
		       var errorTimeKahdN = Mathf.Abs(time-kahdeksasosanuotti);
			   var pointsKahdN = 100*(1-errorTimeKahdN);
			   Maincode.score += pointsKahdN; 
			   Debug.Log("Add points = "+pointsKahdN);
			   
			  
		    }
		   
		}
		if(tweenObject!=null)
		{
			//tweenObject.SendMessage("playAnimation");
			//var tweenPath = tweenObject.GetComponent(typeof("Path1")) as Path1;
		    //tweenPath.playAnimation();
		   
		}
		noteType.GetComponent(Note1).isClean = true;		
		*/
}	
public function tooLongButtonPress() {
    timerOn = false;
    if(noteType!=null)
    {
        if (noteType.name != "kahdeksasosataukoVanha(Clone)")			
        {
            noteType.GetComponent(Note1).isAnimatin = false;
            noteType.GetComponent(Note1).isMoving = true;
            GameObject.Find("BarFull").SendMessage("TimerOff");	

        }
        if (noteType.name != "neljasosataukoVanha(Clone)")			
        {
            noteType.GetComponent(Note1).isAnimatin = false;
            noteType.GetComponent(Note1).isMoving = true;
            GameObject.Find("BarFull").SendMessage("TimerOff");
        }
        if (noteType.name != "PuolitaukoVanha(Clone)")			
        {
            noteType.GetComponent(Note1).isAnimatin = false;
            noteType.GetComponent(Note1).isMoving = true;
            GameObject.Find("BarFull").SendMessage("TimerOff");
        }
        if (noteType.name != "kokotaukoVanha(Clone)")			
        {
            noteType.GetComponent(Note1).isAnimatin = false;
            noteType.GetComponent(Note1).isMoving = true;
            GameObject.Find("BarFull").SendMessage("TimerOff");	
        }
		
        if(noteColliderName == "Collider1")
        {
            tweenObject = GameObject.Find("iTween1");
            tweenObject.SendMessage("setGameobjectForAnimation",noteType);
            tweenObject.SendMessage("playAnimation");
				
        }
        if(noteColliderName == "Collider2")
        {
            tweenObject = GameObject.Find("iTween2");
            tweenObject.SendMessage("setGameobjectForAnimation",noteType);
            tweenObject.SendMessage("playAnimation2");	
							
        }
        if(noteColliderName == "Collider3")
        {
            tweenObject = GameObject.Find("iTween3");
            tweenObject.SendMessage("setGameobjectForAnimation",noteType);
            tweenObject.SendMessage("playAnimation3");	
				
        }
        if(noteColliderName == "Collider4") 
        {
            tweenObject = GameObject.Find("iTween4");
            tweenObject.SendMessage("setGameobjectForAnimation",noteType);
            tweenObject.SendMessage("playAnimation4");	
				
        }			
        noteType.SendMessage("Sad");
    }
        
}
public function changeNoteType(note:GameObject, pathName:String, length:float)
    {
        if(timerOn==false && ignoreRelease==false)
        {	
            if(noteType!=null)
            {
                noteType.GetComponent(Note1).isAnimatin = false;
                noteType.GetComponent(Note1).isMoving = true;
            }
	    
            noteType = note;
            noteColliderName = pathName;
            noteLength = length;
            GameObject.Find("TestCannon").SendMessage("ChangeTarget",note);
        }
    }

public function discardNote(note:GameObject)
        {
            //Debug.Log("ID: "+noteInstanceID);
            if(noteType == note)
            {
                noteType = null;
                noteColliderName = "";
                noteLength = 0.0f;
            }
        }

function Update () {
	if (timerOn==true)
	{
		time = Time.time - startTime;	
		if(time > noteLength+errorMargin)
		{
			tooLongButtonPress();
			ignoreRelease=true;
			timerOn=false;	
		}	
        /*
		if (Input.GetKeyDown ("1"))
		{
		     GameObject.Find("timSpriteSheet_33(Clone)").SendMessage("Sad");
		}
		if (Input.GetKeyDown ("2"))
		{
		     GameObject.Find("timSpriteSheet_33(Clone)").SendMessage("Happy");
		}
		   */    
	}
	
  
}

