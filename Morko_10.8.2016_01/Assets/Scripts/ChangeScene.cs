﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
//using UnityEngine.SceneManagement;  // SceneManager.LoadScene("Concept");

public class ChangeScene : MonoBehaviour
{

   public void LoadLevel_1()
    {
        StartCoroutine("Wait");
    }

	public void LoadLevel_2()
	{
		StartCoroutine("Wait2");
	}

	public void LoadLevel_3()
	{
		StartCoroutine("Wait3");
	}

    IEnumerator Wait()
    {
		Camera.main.SendMessage ("SetAnimationStart");
        yield return new WaitForSeconds(1);
    //    myObject.GetComponent<MyScript>().myFunction);

        Application.LoadLevel("Concept"); //level_1

    }

	IEnumerator Wait2()
	{
		yield return new WaitForSeconds(1);
 //   myObject.GetComponent<MyScript>().myFunction);
		
	//	Application.LoadLevel("Level_2"); 
		
	}

	IEnumerator Wait3()
	{
		yield return new WaitForSeconds(1);
	//    myObject.GetComponent<MyScript>().myFunction);
		
	//	Application.LoadLevel("Level_3"); //
		
	}

    public void LoadMenu()
    {
        Application.LoadLevel("Menu");  
    }

    public void LoadLevelsScene()
    {
        Application.LoadLevel("LevelsScene");
    }
    public void LoadSettings()
	{
		Application.LoadLevel("Settings");	
	}

    public void Quit()
    {
        Application.Quit();
    }
    
}
