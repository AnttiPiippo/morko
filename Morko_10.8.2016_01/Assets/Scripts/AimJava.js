﻿ var goTarget : GameObject;
    var maxDegreesPerSecond : float = 30.0;
    private var qTo : Quaternion;   
      
    function Start () {
     if(goTarget!=null)
     	qTo = goTarget.transform.localRotation;
    }
     
    function Update () {
      if(goTarget!=null){
      
      
    var deltaX = goTarget.transform.position.x - transform.position.x;
    var deltaY = goTarget.transform.position.y - transform.position.y;
    var angle = Mathf.Rad2Deg * Mathf.Atan2(deltaY, deltaX);
  //  Debug.Log("angle: "+angle);
    var rot = transform.rotation.eulerAngles;
 
    if (angle < 45)  //45   
    {
    	angle = 90;
    }
    if (angle > 135)  //135  
    {
    	angle = 90;
    }
    /*
    if(angle >= 315)
    {
	angle = 0;    
    }
    if(angle <= 45)
    {
    angle = 0;
    }
    */
    rot.z = angle-90; 
    transform.rotation.eulerAngles = rot;
    }
}
 public function ChangeTarget (target:GameObject) {
 Debug.Log("TARGET CHANGE");
 	goTarget = target;
 	qTo = goTarget.transform.localRotation;
 }
