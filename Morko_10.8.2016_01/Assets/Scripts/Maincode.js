﻿#pragma strict

static var score = 0; //Player score

static var ZeroStarMin = 0;
static var ZeroStarMax = 2000;

static var OneStarMin = 2001;
static var OneStarMax = 3700;

static var TwoStarMin = 3701;
static var TwoStarMax = 5800;

static var ThreeStarMin = 5801;  
static var ThreeStarMax = 1000000;