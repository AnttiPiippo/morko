﻿using UnityEngine;
using UnityEngine;
using System.Collections;

public class Shake : MonoBehaviour {
	
	
	public Transform cameraTransform;
	public float shakeLength = 1;
	public float shakeTimer;
	public float shakeAmount = 3;
	public float shakeSpeed = 20;
	public bool isShaking = false;
	public bool shakeOnce = false;
	Vector3 originalPos;
	Vector3 newPos;

	public bool temp = false;
	
	void Awake()
	{
		shakeTimer = shakeLength;
	}
	
	void OnEnable()
	{
		originalPos = cameraTransform.position;
	}

	void OnceShake()
	{
		temp = true;
	}

	void Update()
	{
		if (!isShaking && temp) {
			shakeOnce = true;
			shakeTimer = shakeLength;
			newPos = cameraTransform.position;
		}
		
		if (shakeOnce) {
			Shake1 ();
		}
	}
	
	public void Shake1() {
		if (shakeTimer > 0)
		{
			isShaking = true;
			
			if (Vector3.Distance(newPos,cameraTransform.position)<=shakeAmount/30) {newPos = originalPos+Random.insideUnitSphere * shakeAmount;}
			
			cameraTransform.position = Vector3.Lerp(cameraTransform.position, newPos , Time.deltaTime*shakeSpeed);
			
			shakeTimer -= Time.deltaTime;
			temp = false;
		}
		else 
		{
			shakeTimer = 0f;
			cameraTransform.position = originalPos;
			isShaking = false;
			shakeOnce = false;
		}


	}
}