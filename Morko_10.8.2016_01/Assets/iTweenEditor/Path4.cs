﻿using UnityEngine;
using System.Collections;

public class Path4 : MonoBehaviour {
	public GameObject gameObject;
	public bool isPlaying = false;

	public void playAnimation4()
	{
		if (isPlaying == false) {
			isPlaying = true;
			iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath ("Path4"), "time", 2, "easeType", iTween.EaseType.linear));
			Debug.Log ("PLAY ITWEEN ANIMATION 4");
		}
	}

	public void setGameobjectForAnimation(GameObject go)
	{
		gameObject = go;
		isPlaying = false;
	}
	
	void HappyAnimation(GameObject goAnimation)  
	{
		gameObject = goAnimation;
		//    isAnimatingHappy = true;
		//    if (isAnimatingHappy)
		//    {
		gameObject.GetComponent<Animation>().Play("HappyAnimation");
		//    }
		//    isAnimatingHappy = false;   //
	}
	
	void SadAnimation(GameObject goAnimation )
	{
		gameObject = goAnimation;
		//   isAnimatingSad = true;
		//    if (isAnimatingSad)
		//    {
		gameObject.GetComponent<Animation>().Play("SadAnimation");
		//    }
		//    isAnimatingSad = false;  //
	}

}