﻿using UnityEngine;
using System.Collections;

public class Path1 : MonoBehaviour {
	public GameObject gameObject;
	public bool isPlaying = false;

	public void playAnimation()
	{
		if (isPlaying == false) {
			isPlaying = true;
			iTween.MoveTo (gameObject, iTween.Hash ("path", iTweenPath.GetPath ("Path1"), "time", 2, "easeType", iTween.EaseType.linear));
		}
	}
	public void setGameobjectForAnimation(GameObject go)
	{
		gameObject = go;
		isPlaying = false;
	}

	void HappyAnimation(GameObject goAnimation)  
	{
		gameObject = goAnimation;
    //    isAnimatingHappy = true;
    //    if (isAnimatingHappy)
    //    {
       //     gameObject.GetComponent<Animation>().Play("HappyAnimation");
    //    }
    //    isAnimatingHappy = false;  
    }

    void SadAnimation(GameObject goAnimation )
    {
		gameObject = goAnimation;
     //   isAnimatingSad = true;
    //    if (isAnimatingSad)
    //    {
       //     gameObject.GetComponent<Animation>().Play("SadAnimation");
    //    }
    //    isAnimatingSad = false;
    }
}